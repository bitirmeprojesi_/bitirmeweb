﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Entity.Model
{
    public class Post
    {
        public string  Aciklama { get; set; }
        public bool aile { get; set; }
        public bool cocuk { get; set; }
        public string kategori { get; set; }
        public string mekan_tipi { get; set; }
        public bool mekan_tipi_bool { get; set; }
        public bool ogrenci { get; set; }
        public bool sevgili { get; set; }
        public bool yetiskin { get; set; }
        public bool yalniz { get; set; }
        public string photo_url { get; set; }
        public string post_id { get; set; }
        public string user_id { get; set; }
        public long yuklenme_tarihi { get; set; }
        public string gonderiSayisi { get; set; }

        public int sayi { get; set; }
        public string iyi { get; set; }
        public string kotu { get; set; }
        public string notr { get; set; }
        public string user_name { get; set; }
        public string user_profilphoto { get; set; }
        public string comment_user_name { get; set; }
        public string comment_user_profilphoto { get; set; }

        public virtual Dictionary<string, Comments> Comment { get; set; }
    }
}
