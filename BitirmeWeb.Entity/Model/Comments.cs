﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Entity.Model
{
   public class Comments

    { 
       
        public string begenenler { get; set; }
        public string user_id { get; set; }
        public string yorum { get; set; }
        public string yorum_begeni { get; set; }
        public long yorum_tarih { get; set; }
        public string hash_code { get; set; }

        public string user_name { get; set; }
        public string profilResmi { get; set; }

    }
}
