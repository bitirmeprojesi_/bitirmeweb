﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Entity.Model
{
  public class Register
    {
        public string email { get; set; }
        public string email_phone_number { get; set; }
        public string name_surname { get; set; }       
        public string password { get; set; }
        public string phone_number { get; set; }
        //user_detail kısmı
        public string aciklama { get; set; }
        public string gonderiSayisi { get; set; }
        public string profilResmi { get; set; }
        public string takipEdilenSayisi { get; set; }
        public string takipciSayisi { get; set; }
        public string webSitesi { get; set; }

        public string user_id { get; set; }
        public string user_name { get; set; }
    }
}
