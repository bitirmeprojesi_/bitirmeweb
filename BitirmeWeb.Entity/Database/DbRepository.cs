﻿using FireSharp.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Entity.Database 
{
    //AnaRepository tüm repositoryler burdan kalıtım alır. burada list add update ve delete işlemlerinin genel yapısı vardır.
    public class DbRepository<T> : dbBaglanti
    {
        public Dictionary<string, T> GetAll(string path)
        {
            client = new FireSharp.FirebaseClient(config);

            FirebaseResponse re = client.Get(path);
            Dictionary<string, T> obj = re.ResultAs<Dictionary<string, T>>();

            return obj;
        }
        public Dictionary<string, string> GetString(string path)
        {
            client = new FireSharp.FirebaseClient(config);

            FirebaseResponse re = client.Get(path);
            Dictionary<string, string> obj = re.ResultAs<Dictionary<string, string>>();

            return obj;
        }

        public T Get(string path, string id)
        {
            client = new FireSharp.FirebaseClient(config);

            FirebaseResponse re = client.Get(path+id);
            T obj = re.ResultAs<T>();
            return obj;
        }
        public T Gets(string path)
        {
            client = new FireSharp.FirebaseClient(config);

            FirebaseResponse re = client.Get(path);
            T obj = re.ResultAs<T>();
            return obj;
        }
      
        public T GetDetail(string path, string id,string detail)
        {
            client = new FireSharp.FirebaseClient(config);

            FirebaseResponse re = client.Get(path + id + detail);
            T obj = re.ResultAs<T>();
            return obj;
        }

        public T Add(T model, string path)
        {

            client = new FireSharp.FirebaseClient(config);

            SetResponse response = client.Set(path, model);
            T result = response.ResultAs<T>();

            return result;
        }

        public bool AddString(string model, string path)
        {
            bool result = false;
            client = new FireSharp.FirebaseClient(config);

            SetResponse response = client.Set(path, model);
            result = true;

            return result;
        }
        
        public T Update(T model, string path)
        {

            client = new FireSharp.FirebaseClient(config);

            FirebaseResponse response = client.Update(path, model);
            T result = response.ResultAs<T>();

            return result;
        }
        public void Deletee(string path)
        {
            client = new FireSharp.FirebaseClient(config);
            DeleteResponse response = client.Delete(path);
          
        }
        public bool Delete(string path)
        {
            bool result = false;
            client = new FireSharp.FirebaseClient(config);
            DeleteResponse response = client.Delete(path);
            result = true;
            return result;
        }

      
    }
}
