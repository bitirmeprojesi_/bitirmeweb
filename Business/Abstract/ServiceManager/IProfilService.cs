﻿using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Abstract.ServiceManager
{
    public interface IProfilService
    {
        bool Update(string id, Register user);
        bool kullaniciadiKontrol(string user,string user_id,string phone_number);
    }
}
