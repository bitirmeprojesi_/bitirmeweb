﻿using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Abstract.ServiceManager
{
    public interface IProfilPostService 
    {
        Dictionary<string, Post> GetPostList(string user_id);
        Post GetPost(string user_id,string post_id);
        bool OnayKaydet(string user_id, string post_id,string durum);

        Dictionary<string, Comments> GetCommentList(string post_id);
        Comments addComment(string post_id,string user_id, string yorum, string yorum_begeni, long yorum_tarih,string hash_code);
        Register getUserInfo(string userId);
        bool addLike(string post_id, string user_id, string yorum, string yorum_begeni, long yorum_tarih, string hash_code);
        void deletePosts(string user_id,string post_id,string gonderiSayisi);
        Post updatePostNumber(string user_id,string post_id,string number);
        Dictionary<string, string> GetFollowers(string user_id,string type);
        Dictionary<string, string> userList(string []user_id);
    }
}
