﻿using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Abstract.ServiceManager
{
    public interface IAccountService
    {
        Register _add(string email, string email_phone_number, string password,string name,string phone_number,string aciklama,string gönderiSayisi,
            string takipEdilenSayisi,string takipciSayisi,string webSitesi, string userId, string username);

        Register getUsers(string userId,string email,string password,string phone_number);
        Register user(string path, string id);
        Register user_detail(string path, string id);
       
    }
}
