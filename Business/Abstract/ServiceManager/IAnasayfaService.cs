﻿using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Abstract.ServiceManager
{
    public interface IAnasayfaService
    {
        Dictionary<string, Post> UserPost(string[] usersId, int pagesize);
        Dictionary<string, Comments> GetComments(string post_id);
        Dictionary<string, string> GetFollowing(string user_id);

    }
}
