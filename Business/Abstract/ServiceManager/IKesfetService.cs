﻿using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Abstract.ServiceManager
{
    public interface IKesfetService
    {
        Dictionary<string, Post> AllPost(string userId, string[] usersId);
    }
}
