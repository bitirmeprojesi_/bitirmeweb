﻿using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Abstract.ServiceManager
{
    public interface IPostService
    {
        Dictionary<string, Post> GetPostList(string id);
    }
}
