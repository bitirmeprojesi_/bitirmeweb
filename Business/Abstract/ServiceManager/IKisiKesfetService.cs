﻿using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Abstract.ServiceManager
{
    public interface IKisiKesfetService
    {
        Dictionary<string, Register> AllUsers(string userId, string[] usersId);
        bool following(string userId, string followId);
    }
}
