﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.Repository;
using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Concrete.ServiceManager
{
    public class KesfetServiceManager : IKesfetService
    {
        PostRepository postRep = new PostRepository();
        CommentRespository commentRep = new CommentRespository();
        RegisterRepository registerRep = new RegisterRepository();

        public Dictionary<string, Post> AllPost(string userId, string[] usersId)
        {
            var post = new Dictionary<string, Post>();
            var posts = postRep.GetAll("posts/");
            var evet = false;
            foreach (var item in posts)  
            {
                foreach (var item2 in usersId)
                {
                    evet = false;
                    if (item.Key == item2)
                    {
                        evet = true;
                        break;
                    }
                    else if (item.Key == userId)
                    {
                        evet = true;
                        break;
                    }
                }
                if (evet == false)
                {
                    var data = postRep.GetAll("posts/" + item.Key);
                    foreach (var item3 in data)
                    {
                        post.Add(item3.Key, item3.Value);
                    }
                }
                
            }
            return post;
        }
    }
}
