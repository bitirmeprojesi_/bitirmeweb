﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.Repository;
using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bitirme.Business.Concrete.ServiceManager
{
    public class AnasayfaServiceManager : IAnasayfaService
    {
        PostRepository postRep = new PostRepository();
        CommentRespository commentRep = new CommentRespository();
        RegisterRepository registerRep = new RegisterRepository();
        FollowingRepository folrep = new FollowingRepository();
        public Dictionary<string, Comments> GetComments(string post_id)
        {
            var commentlist = commentRep.GetAll("comments/" + post_id);
            return commentlist;
        }

        public Dictionary<string, string> GetFollowing(string user_id)
        {
            var followings = folrep.GetString("following/" + user_id);
            return followings;
        }

        public Dictionary<string, Post> UserPost(string[] usersId, int pageIndex)
        {
            var post = new Dictionary<string, Post>();
            var post2 = new Dictionary<string, Post>();
            var posts = postRep.GetAll("posts/");

            foreach (var item in posts)
            {
                foreach (var item2 in usersId)
                {
                    var user_name = registerRep.Get("users/", item2);
                    var user_photo = registerRep.Gets("users/" + item2 + "/user_detail");
                    if (item.Key == item2)
                    {
                        var data = postRep.GetAll("posts/" + item2);
                        foreach (var item3 in data)
                        {
                            if(item3.Value.mekan_tipi=="Kapali Alan")
                            {
                                item3.Value.mekan_tipi_bool = true;
                            }
                            item3.Value.user_name = user_name.user_name;
                            item3.Value.user_profilphoto = user_photo.profilResmi;

                            post.Add(item3.Key, item3.Value);

                        }


                    }

                }
            }

            foreach (var item in post.OrderByDescending(x=>x.Value.yuklenme_tarihi))
            {
                post2.Add(item.Key, item.Value);
            }

            return post2;

        }
    }
}
