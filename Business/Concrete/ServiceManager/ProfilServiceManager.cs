﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.Repository;
using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Concrete.ServiceManager
{
    public class ProfilServiceManager:IProfilService
    {
        RegisterRepository _dataRep = new RegisterRepository();

        public bool kullaniciadiKontrol(string user,string user_id,string phone_number)
        {
            var userlist = _dataRep.GetAll("users");
            var varmı = false;
            foreach (var item in userlist)
            {
                if (item.Value.user_name == user && item.Value.user_id != user_id)
                {
                    varmı = true;
                    return varmı;
                }
                if (item.Value.phone_number == phone_number && item.Value.user_id != user_id)
                {
                    varmı = true;
                    return varmı;
                }
            }
            return varmı;
        }
        public bool Update(string id, Register user)
        {
            bool result = false;
            var path = "users/";
            var model = _dataRep.Get(path, id);
            var detail = _dataRep.GetDetail(path, id, "/user_detail");

            model.name_surname = user.name_surname;
            model.password = user.password;
            model.user_name = user.user_name;
            detail.webSitesi = user.webSitesi;
            detail.profilResmi = user.profilResmi;
            detail.aciklama = user.aciklama;
            if (user.phone_number != null)
            {
                model.phone_number = user.phone_number;
            }
            
            _dataRep.Update(model, "users/" + model.user_id);
            _dataRep.Update(detail, "users/" + model.user_id+ "/user_detail/");
            result = true;
            return result;
        }
    }
}
