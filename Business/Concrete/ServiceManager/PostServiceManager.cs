﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.Repository;
using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Concrete.ServiceManager
{
    public class PostServiceManager : IPostService
    {
        PostRepository postRep = new PostRepository();

        public Dictionary<string, Post> GetPostList(string id)
        {
            return postRep.GetAll("post"+id);
        }
    }
}
