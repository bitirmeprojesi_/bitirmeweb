﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.Repository;
using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Concrete.ServiceManager
{
    public class AccountServiceManager : IAccountService
    {
        RegisterRepository _dataRep = new RegisterRepository();
        FollowingRepository followingRep = new FollowingRepository();

        public Register user(string path,string id)
        {
            var register = _dataRep.Get(path, id);
            return register;
        }
        public Register user_detail(string path,string id)
        {
            var register = _dataRep.GetDetail(path, id,"/user_detail");
            var sayi= followingRep.GetString("following/" + id);
            var sayi2 = followingRep.GetString("follower/" + id);
            register.takipEdilenSayisi = sayi.Count.ToString();
            register.takipciSayisi = sayi2.Count.ToString();
            return register;
        }

        public Register getUsers(string userId, string email, string password,string phone_number)
        {
            var registerData = new Register();
            var a= _dataRep.Get("users/", userId);
            var userlist = _dataRep.GetAll("users");

            if (userId == null)
            {
                foreach (var item in userlist)
                {
                    if (item.Value.user_name == email)
                    {
                        var data= _dataRep.Get("users/", item.Value.user_id);
                        registerData = data;
                        return data;
                    }
                    else if(item.Value.phone_number == email)
                    {
                        var data = _dataRep.Get("users/", item.Value.user_id);
                        registerData = data;
                        return data;
                    }
                }
            }
            else
            {
                if (a != null)
                {
                    registerData = a;
                    return a;
                }

                else
                {
                    return null;
                }
                
            }

            return registerData;
        }

        public Register _add(string email, string email_phone_number, string password, string name, string phone_number, string aciklama, string gönderiSayisi,
            string takipEdilenSayisi, string takipciSayisi, string webSitesi, string userId, string username)
        {

            var master = new Register
            {
                email = email,
                email_phone_number = "",
                password = password,
                phone_number = "",
                name_surname = name,
                user_name = username,
                user_id = userId
            };
            _dataRep.Add(master, "users/" + userId + "/");

            var detail = new Register
            {
                aciklama = "",
                gonderiSayisi = gönderiSayisi,
                takipEdilenSayisi = takipEdilenSayisi,
                takipciSayisi = takipciSayisi,
                webSitesi = "",
                profilResmi = " ",
            };
          return  _dataRep.Add(detail, "users/" + userId + "/user_detail/");
             
        }

    }
}
