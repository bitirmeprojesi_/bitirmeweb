﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.Repository;
using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bitirme.Business.Concrete.ServiceManager
{
    public class KisiKesfetServiceManager : IKisiKesfetService
    {

        RegisterRepository registerRep = new RegisterRepository();
        FollowingRepository followingRep = new FollowingRepository();

        public Dictionary<string, Register> AllUsers(string userId, string[] usersId)
        {
            var user = new Dictionary<string, Register>();
            var users = registerRep.GetAll("users/");
            var evet = false;
            var sayac = "0";var sayac2 = 0;
            foreach (var item in users)
            {
                foreach (var item2 in usersId)
                {
                    evet = false;
                    if (item.Key == item2)
                    {
                        evet = true;
                        break;
                    }
                    else if (item.Key == userId)
                    {
                        evet = true;
                        break;
                    }
                }
                if (evet == false)
                {
                    sayac2++;
                    var data = registerRep.Gets("users/" + item.Key);
                    var user_detail_data = registerRep.Gets("users/" + item.Key+"/user_detail");
                    data.profilResmi = user_detail_data.profilResmi;
                    data.takipciSayisi = user_detail_data.takipciSayisi;
                    data.gonderiSayisi = user_detail_data.gonderiSayisi;
                    user.Add(sayac+sayac2,data);
                }

            }
            return user;
        }

        public bool following(string userId, string followId)
        {
            return followingRep.AddString(followId, "following/"+userId+"/"+ followId);
        }
    }
}
