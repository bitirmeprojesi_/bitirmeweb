﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.Repository;
using Bitirme.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bitirme.Business.Concrete.ServiceManager
{
    public class ProfilPostServiceManager : IProfilPostService
    {
        PostRepository postRep = new PostRepository();
        OnaylaRepository onayRep = new OnaylaRepository();
        CommentRespository commentRep = new CommentRespository();
        RegisterRepository registerRepository = new RegisterRepository();
        FollowingRepository followingRepository = new FollowingRepository();

        public bool addLike(string post_id, string user_id, string yorum, string yorum_begeni, long yorum_tarih, string hash_code)
        {
            int likecount = Convert.ToInt32(yorum_begeni) + 1;
            var like = new Comments
            {
                user_id = user_id,
                yorum = yorum,
                yorum_begeni = Convert.ToString(likecount),
                yorum_tarih = yorum_tarih
            };

            commentRep.Add(like, "comments/" + post_id + "/" + hash_code + "/");
            return commentRep.AddString(user_id.ToString(), "comments/" + post_id + "/" + hash_code + "/begenenler/" + user_id);

        }
        public Register getUserInfo(string userId)
        {
            var userInfo = new Register();
            var datamaster = registerRepository.Get("users/", userId);
            var path = "users/" + userId + "/user_detail/";
            var datadetail = registerRepository.Gets(path);
            if (userId != null)
            {
                userInfo.user_name = datamaster.user_name;
                userInfo.profilResmi = datadetail.profilResmi;
            }
            return userInfo;
        }



        public Comments addComment(string post_id, string user_id, string yorum, string yorum_begeni, long yorum_tarih, string hash_code)
        {

            DateTime foo = DateTime.UtcNow;
            yorum_tarih = ((DateTimeOffset)foo).ToUnixTimeSeconds();

            var comments = new Comments
            {
                user_id = user_id,
                yorum = yorum,
                yorum_begeni = yorum_begeni,
                yorum_tarih = yorum_tarih
            };


            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[19];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            hash_code = new String(stringChars);
            hash_code = "-" + hash_code;

            return commentRep.Add(comments, "comments/" + post_id + "/" + hash_code + "/");

        }



        public Dictionary<string, Comments> GetCommentList(string post_id)
        {
            var comments = commentRep.GetAll("comments/");
            var commentlist = new Dictionary<string, Comments>();
            foreach (var item in comments)
            {
                if (item.Key == post_id)
                {
                    var list = commentRep.GetAll("comments/" + post_id).OrderByDescending(x=>x.Value.yorum_tarih);
                    foreach (var item2 in list)
                    {
                        var user = registerRepository.Gets("users/" + item2.Value.user_id);
                        var userdetail = registerRepository.Gets("users/" + item2.Value.user_id+"/user_detail");
                        item2.Value.user_name = user.user_name;
                        item2.Value.profilResmi = userdetail.profilResmi;
                        commentlist.Add(item2.Key, item2.Value);
                    }

                }
            }
            return commentlist;
        }


        public Post GetPost(string user_id, string post_id)
        {
            int iyi = 0; int kotu = 0; int notr = 0;
            var tumonay = onayRep.GetAll("onaylar/");
            var datamaster = registerRepository.Get("users/", user_id);
            var path = "users/" + user_id + "/user_detail/";
            var datadetail = registerRepository.Gets(path);

            var post = new Post();
            foreach (var item in tumonay)
            {
                if (item.Key == post_id)
                {
                    var onay = onayRep.GetAll("onaylar/" + post_id);
                    foreach (var item2 in onay)
                    {
                        if (item2.Key == "iyi")
                        {
                            iyi++;
                        }
                        if (item2.Key == "kotu")
                        {
                            kotu++;
                        }
                        if (item2.Key == "notr")
                        {
                            notr++;
                        }
                    }
                    post = postRep.Get("posts/" + user_id + "/", post_id);
                    post.iyi = iyi.ToString();
                    post.kotu = kotu.ToString();
                    post.notr = notr.ToString();
                    post.gonderiSayisi = datadetail.gonderiSayisi;
                    return post;
                }
                else
                {
                    post = postRep.Get("posts/" + user_id + "/", post_id);
                    post.iyi = "0";
                    post.kotu = "0";
                    post.notr = "0";
                    post.gonderiSayisi = datadetail.gonderiSayisi;
                    return post;
                }
            }
            return post;
        }

        public Dictionary<string, Post> GetPostList(string user_id)
        {
            var post = new Dictionary<string, Post>();
            var postlist = postRep.GetAll("posts/" + user_id).OrderByDescending(x=>x.Value.yuklenme_tarihi);
            foreach (var item in postlist)
            {
                post.Add(item.Key, item.Value);
            }
            return post;
        }


        public bool OnayKaydet(string user_id, string post_id, string durum)
        {
            var varmı = false;
            var iyi = false;
            var kotu = false;
            var notr = false;
            var onay = onayRep.GetAll("onaylar/");
            foreach (var item in onay)
            {
                if (item.Key == post_id)
                {
                    varmı = true;
                }
            }
            if (varmı)
            {
                var onaydurum = onayRep.GetAll("onaylar/" + post_id);
                foreach (var item in onaydurum)
                {
                    if (item.Key == "iyi")
                    {
                        iyi = true;
                    }
                    if (item.Key == "kotu")
                    {
                        kotu = true;
                    }
                    if (item.Key == "notr")
                    {
                        notr = true;
                    }
                }
                if (durum == "iyi")
                {
                    if (kotu)
                    {
                        onayRep.Delete("onaylar/" + post_id + "/kotu/" + user_id);
                    }
                    if (notr)
                    {
                        onayRep.Delete("onaylar/" + post_id + "/notr/" + user_id);
                    }
                    return onayRep.AddString(user_id.ToString(), "onaylar/" + post_id + "/iyi/" + user_id);
                }
                else if (durum == "kotu")
                {
                    if (iyi)
                    {
                        onayRep.Delete("onaylar/" + post_id + "/iyi/" + user_id);
                    }
                    if (notr)
                    {
                        onayRep.Delete("onaylar/" + post_id + "/notr/" + user_id);
                    }
                    return onayRep.AddString(user_id.ToString(), "onaylar/" + post_id + "/kotu/" + user_id);
                }
                else if (durum == "notr")
                {
                    if (kotu)
                    {
                        onayRep.Delete("onaylar/" + post_id + "/kotu/" + user_id);
                    }
                    if (iyi)
                    {
                        onayRep.Delete("onaylar/" + post_id + "/iyi/" + user_id);
                    }
                    return onayRep.AddString(user_id.ToString(), "onaylar/" + post_id + "/notr/" + user_id);
                }
            }
            else
            {
                if (durum == "iyi")
                {
                    return onayRep.AddString(user_id.ToString(), "onaylar/" + post_id + "/iyi/" + user_id);
                }
                else if (durum == "kotu")
                {
                    return onayRep.AddString(user_id.ToString(), "onaylar/" + post_id + "/kotu/" + user_id);
                }
                else if (durum == "notr")
                {
                    return onayRep.AddString(user_id.ToString(), "onaylar/" + post_id + "/notr/" + user_id);
                }
            }

            return true;
        }

        public void deletePosts(string user_id, string post_id, string gonderiSayisi)
        {
            updatePostNumber(user_id, post_id, gonderiSayisi);
            postRep.Deletee("comments/" + post_id);
            postRep.Deletee("onaylar/" + post_id);
            postRep.Deletee("posts/" + user_id + "/" + post_id);
        }

        public Post updatePostNumber(string user_id, string post_id, string number)
        {
            var datamaster = postRep.Get("users/", user_id);
            var path = "users/" + user_id + "/user_detail/";
            var datadetail = postRep.Gets(path);

            var datam = Convert.ToInt16(datadetail.gonderiSayisi) - 1;
            datadetail.gonderiSayisi = datam.ToString();

            return postRep.Update(datadetail, path);
        }

        public Dictionary<string, string> GetFollowers(string user_id, string type)
        {
            var path = "";
            if (type == "followers")
            {
                path = "follower/" + user_id;

            }
            if (type == "followings")
            {
                path = "following/" + user_id;

            }
            var list = followingRepository.GetString(path);
            return list;
        }

        public Dictionary<string, string> userList(string[] user_id)
        {
            int i = 0;
            var users = new Dictionary<string, string>();
            var userList = registerRepository.GetAll("users/");

            foreach (var item in userList)
            {
                if(item.Key.ToString()==user_id[i])
                {
                    users.Add(item.Key, item.Value.user_name);
                    i++;
                }

              
            }
            return users;
        }
    }
}
