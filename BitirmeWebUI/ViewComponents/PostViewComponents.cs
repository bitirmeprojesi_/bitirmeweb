﻿using Bitirme.Business.Abstract.ServiceManager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bitirme.WebUI.ViewComponents
{
    public class PostViewComponents :ViewComponent
    {
        private IProfilPostService postService;
        public PostViewComponents(IProfilPostService postService)
        {
            this.postService = postService;
        }
        public IViewComponentResult Invoke(string user_id, string post_id)
        {
            var post = postService.GetPost(user_id, post_id);
            return View(post);
        }
    }
}
