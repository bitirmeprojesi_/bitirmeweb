﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Entity.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bitirme.WebUI.ViewComponents
{
    public class ProfilViewComponents : ViewComponent
    {
        private IAccountService registerService;
        public ProfilViewComponents(IAccountService registerService)
        {
            this.registerService = registerService;
        }
        public IViewComponentResult Invoke(string id)
        {
            var register = registerService.user("users/", id);
            var detail = registerService.user_detail("users/", id);
            register.takipciSayisi = detail.takipciSayisi;
            register.takipEdilenSayisi = detail.takipEdilenSayisi;
            register.gonderiSayisi = detail.gonderiSayisi;
            register.profilResmi = detail.profilResmi;
            return View(register);
        }
    }
}
