﻿using Bitirme.Business.Abstract.ServiceManager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bitirme.WebUI.ViewComponents
{
    public class ProfilPostViewComponents : ViewComponent
    {
        private IProfilPostService postService;
        public ProfilPostViewComponents(IProfilPostService postService)
        {
            this.postService = postService;
        }
        public IViewComponentResult Invoke(string user_id)
        {
            var list=postService.GetPostList(user_id);
            return View(list);
        }
    }
}
