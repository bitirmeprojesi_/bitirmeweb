﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Entity.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bitirme.WebUI.ViewComponents
{
    public class NavbarViewComponents : ViewComponent
    {
        private IAccountService registerService;
        public NavbarViewComponents(IAccountService registerService)
        {
            this.registerService = registerService;
        }
        public IViewComponentResult Invoke(string id)
        {
            if (id == " "){

                return View();
            }
            else
            {
                var register = registerService.user("users/", id);
                return View(register);
            }
            
        }
    }
}
