﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Entity.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bitirme.WebUI.ViewComponents
{
    public class AnasayfaPostViewComponents : ViewComponent
    {
        private IAnasayfaService anasayfaService;
        public AnasayfaPostViewComponents(IAnasayfaService anasayfaService)
        {
            this.anasayfaService = anasayfaService;
        }
        public IViewComponentResult Invoke(int sayi, bool aile, bool cocuk, bool ogrenci, bool sevgili, bool yetiskin, bool yalniz,bool mekan_tipi_bool)
        {
            var post = new Post();
            post.aile = aile;
            post.cocuk = cocuk;
            post.ogrenci = ogrenci;
            post.sevgili = sevgili;
            post.yetiskin = yetiskin;
            post.yalniz = yalniz;
            post.mekan_tipi_bool = mekan_tipi_bool;
            post.sayi = sayi;
            return View(post);
        }
    }
}
