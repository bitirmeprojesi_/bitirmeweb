﻿using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Entity.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bitirme.WebUI.ViewComponents
{
    public class KesfetPostViewComponents : ViewComponent
    {
        private IKesfetService kesfetService;
        public KesfetPostViewComponents(IKesfetService kesfetService)
        {
            this.kesfetService = kesfetService;
        }
        public IViewComponentResult Invoke(string userId)
        {
            var list =new Post();
            list.user_id = userId;
            return View(list);
        }
    }
}
