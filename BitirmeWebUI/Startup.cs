﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Business.Concrete.ServiceManager;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bitirme.WebUI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IDeneme, DenemeService>();
            services.AddSingleton<IAccountService, AccountServiceManager>();
            services.AddSingleton<IProfilService, ProfilServiceManager>();
            services.AddSingleton<IProfilPostService, ProfilPostServiceManager>();
            services.AddSingleton<IAnasayfaService, AnasayfaServiceManager>();
            services.AddSingleton<IKesfetService, KesfetServiceManager>();
            services.AddSingleton<IKisiKesfetService, KisiKesfetServiceManager>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStatusCodePages();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                   name: "default",
                   template: "{controller=Account}/{action=Login}/{id?}");
            });

        }
    }
}
