﻿firebase.auth().onAuthStateChanged(function (user) { 
    if (user) {
        // User is signed in.
        var user = firebase.auth().currentUser;

        if (user != null) {

            var user_mail = user.email;
            var user_id = user.uid;
            var email_verified = user.emailVerified;

            if (email_verified) {
                document.getElementById("user_div").innerHTML = "Welcome User : " + user_mail;
            }
            else {
                send_code();

                //document.getElementById("user_div").innerHTML = "Verifate account : " + user_mail;
            }
        }

    } else {
        // No user is signed in. 

    }
});


function login() {
   

    var userEmail = document.getElementById("divEmail").querySelector("#EMail").value;
    var userPass = document.getElementById("divPass").querySelector("#Password").value;

    if (userEmail.includes("@")) {
        firebase.auth().signInWithEmailAndPassword(userEmail, userPass)
            .then(function () {
                var user = firebase.auth().currentUser;
                var userId = user.uid;
                var email_verified = user.emailVerified;
                if (email_verified) {
                    $.ajax({
                        url: '/Account/GetUser',
                        data: "userId=" + userId + "&email=" + userEmail + "&password=" + userPass,
                        type: "GET",
                        timeout: 62000,
                        success: function (data) {
                            window.location.href = "/Home/Index?userId=" + userId;
                        },
                        error: function (passParams) {

                        }
                    });
                }

                if (!email_verified) {
                    alert("Verificate your account.");
                }
            })

            .catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                window.alert("Hata : " + errorMessage);
            });
    }


    else {
        $.ajax({
            url: '/Account/GetUser',
            data: "email=" + userEmail,
            type: "GET",
            timeout: 12000,
            success: function (data) { 
                if (userEmail == data.user_name) { 
                    firebase.auth().signInWithEmailAndPassword(data.email, userPass)
                        .then(function () {
                            var user = firebase.auth().currentUser;
                            var userId = user.uid;
                            var email_verified = user.emailVerified;
                            if (email_verified) {
                                $.ajax({
                                    url: '/Account/GetUser',
                                    data: "userId=" + userId + "&email=" + data.email + "&password=" + userPass,
                                    type: "GET",
                                    timeout: 12000,
                                    success: function (data) {
                                        $.ajax({
                                            url: '/Home/Index',
                                            data: "userId=" + userId,
                                            type: "GET",
                                            timeout: 12000,
                                            success: function (data) {
                                                window.location.href = "/Home/Index?userId=" + userId;
                                            },
                                            error: function (passParams) {

                                            }
                                        });

                                    },
                                    error: function (passParams) {

                                    }
                                });
                            }

                            if (!email_verified) {
                                alert("Verificate your account.");
                            }
                        })

                        .catch(function (error) {
                            // Handle Errors here.
                            var errorCode = error.code;
                            var errorMessage = error.message;
                            window.alert("Error : " + errorMessage);
                        });
                }
                else {
                    if (data.email_phone_number != "") {
                        userEmail = data.email_phone_number;
                        firebase.auth().signInWithEmailAndPassword(userEmail, userPass)
                            .then(function () { 
                                var user = firebase.auth().currentUser;
                                var userId = user.uid;
                                userEmail = userEmail.split("@");
                                //if (email_verified) {
                                $.ajax({
                                    url: '/Account/GetUser',
                                    data: "userId=" + userId + "&email=" + "" + "&password=" + userPass + "&phone_number=" + userEmail[0],
                                    type: "GET",
                                    timeout: 12000,
                                    success: function (data) {
                                        $.ajax({
                                            url: '/Home/Index',
                                            data: "userId=" + userId,
                                            type: "GET",
                                            timeout: 12000,
                                            success: function (data) {
                                                window.location.href = "/Home/Index?userId=" + userId;
                                            },
                                            error: function (passParams) {

                                            }
                                        });

                                    },
                                    error: function (passParams) {

                                    }
                                });
                            })

                            .catch(function (error) {
                                // Handle Errors here.
                                var errorCode = error.code;
                                var errorMessage = error.message;
                                window.alert("Error : " + errorMessage);

                                // ...
                            });
                    }
                    else {
                        userEmail = data.email;
                        firebase.auth().signInWithEmailAndPassword(userEmail, userPass)
                            .then(function () { 
                                var user = firebase.auth().currentUser;
                                var userId = user.uid;
                                userEmail = userEmail.split("@");
                                //if (email_verified) {
                                $.ajax({
                                    url: '/Account/GetUser',
                                    data: "userId=" + userId + "&email=" + "" + "&password=" + userPass + "&phone_number=" + userEmail[0],
                                    type: "GET",
                                    timeout: 12000,
                                    success: function (data) {
                                        $.ajax({
                                            url: '/Home/Index',
                                            data: "userId=" + userId,
                                            type: "GET",
                                            timeout: 12000,
                                            success: function (data) {
                                                window.location.href = "/Home/Index?userId=" + userId;
                                            },
                                            error: function (passParams) {
                                                // code here
                                            }
                                        });

                                        //   window.location("Home/Index/");
                                    },
                                    error: function (passParams) {
                                        // code here
                                    }
                                });
                                // }

                                //if (!email_verified) {
                                //    alert("Verificate your account.");
                                //}
                            })

                            .catch(function (error) {
                                // Handle Errors here.
                                var errorCode = error.code;
                                var errorMessage = error.message;
                                window.alert("Error : " + errorMessage);

                                // ...
                            });
                    }
                }
            },
            error: function (passParams) {
                // code here
            }
        });
    }
    
}

function logout() {
    firebase.auth().signOut();
    window.location.href = "/Account/Login";
}


function register() {
    var userEmail = document.getElementById("divEmail").querySelector("#EMail").value;

    var userPass = document.getElementById("divPass").querySelector("#Password").value;
    firebase.auth().createUserWithEmailAndPassword(userEmail, userPass)
        .catch(function (error) {
            // Handle Errors here. 
            var errorCode = error.code;
            var errorMessage = error.message;
            // ...
        });
}


function send_code() {
    var user = firebase.auth().currentUser;
    user.sendEmailVerification()
        .then(function () {
            debugger;
            var user_mail = user.email;
            var user_id = user.uid;
            var email_verified = user.emailVerified;

            var userPass = document.getElementById("divPass").querySelector("#Password").value;
            var name = document.getElementById("divName").querySelector("#Name").value;
            var userName = document.getElementById("divUserName").querySelector("#userName").value;
            // Email sent. 
            $.ajax({
                url: '/Account/Add',
                data: {
                    email: user_mail,
                    email_phone_number: " ",
                    password: userPass,
                    name: name,
                    phone_number: " ",
                    aciklama: " ",
                    gönderiSayisi: "0",
                    takipEdilenSayisi: "0",
                    takipciSayisi: "0",
                    webSitesi: " ",
                    userId: user_id,
                    username: userName,
                },
                type: "POST",
                success: function (data) {
                  
                    alert("üye olundu");
                },
                error: function (passParams) {
                    // code here
                }
            });
            alert("Verification gönderildi.")
        })
        .catch(function (error) {
            // An error happened.
            alert("Error : " + error.message);
        });
}


