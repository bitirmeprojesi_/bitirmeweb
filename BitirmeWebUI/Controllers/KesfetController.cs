﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bitirme.Business.Abstract.ServiceManager;
using Microsoft.AspNetCore.Mvc;

namespace Bitirme.WebUI.Controllers
{
    public class KesfetController : Controller
    {
        private IAccountService accountService;
        private IKesfetService kesfetService;

        public KesfetController(IAccountService accountService, IKesfetService kesfetService)
        {
            this.accountService = accountService;
            this.kesfetService = kesfetService;

        }

        public IActionResult Index(string userId)
        {
            var a = accountService.user("users/", userId);
            return View(a);
        }

        [HttpPost]
        public JsonResult allPost(string userId, string[] usersId)
        {
            return Json(kesfetService.AllPost(userId,usersId));
        }
        public IActionResult allPostComponents(string userId)
        {
            return ViewComponent("KesfetPostViewComponents",new { userId});
        }

    }
}