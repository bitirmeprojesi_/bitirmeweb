﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Entity.Model;
using Microsoft.AspNetCore.Mvc;

namespace Bitirme.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IAccountService accountService;
        private IAnasayfaService anasayfaService;
        public HomeController(IAccountService accountService, IAnasayfaService anasayfaService)
        {
            this.accountService = accountService;
            this.anasayfaService = anasayfaService;
        }
      
        public IActionResult Index(string userId)
        {
            var a = accountService.user("users/", userId);
            return View(a);
        }
        [HttpPost]
        public JsonResult GetUserPost(string[] usersId,int? page, int pageIndex)
        {
            var list = new Dictionary<string, Post>();
            list = anasayfaService.UserPost(usersId, pageIndex);
            return Json(list);
        }

        public IActionResult GetPost(int sayi, bool aile, bool cocuk, bool ogrenci, bool sevgili, bool yetiskin, bool yalniz,bool mekan_tipi_bool)
        {
            return ViewComponent("AnasayfaPostViewComponents",new { sayi, aile, cocuk, ogrenci, sevgili, yetiskin,yalniz,mekan_tipi_bool });
        }

        public JsonResult GetFollowing(string user_id)
        {
            var followings = anasayfaService.GetFollowing(user_id);
            return Json(followings);
        }

    }
}