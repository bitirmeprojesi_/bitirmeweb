﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bitirme.Business.Abstract.ServiceManager;
using Microsoft.AspNetCore.Mvc;

namespace Bitirme.WebUI.Controllers
{
    public class KisiKesfetController : Controller
    {
        private IAccountService accountService;
        private IKisiKesfetService kisikesfetService;

        public KisiKesfetController(IAccountService accountService, IKisiKesfetService kisikesfetService)
        {
            this.accountService = accountService;
            this.kisikesfetService = kisikesfetService;

        }

        public IActionResult Index(string userId)
        {
            var a = accountService.user("users/", userId);
            return View(a);
        }

        [HttpPost]
        public JsonResult following(string userId, string followId)
        {
            return Json(kisikesfetService.following(userId, followId));
        }

        [HttpPost]
        public JsonResult allPost(string userId, string[] usersId)
        {
            return Json(kisikesfetService.AllUsers(userId, usersId));
        }
    }
}