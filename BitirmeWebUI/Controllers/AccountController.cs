﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Entity.Model;
using Microsoft.AspNetCore.Mvc;

namespace Bitirme.WebUI.Controllers
{
    public class AccountController : Controller
    {
        private IAccountService registerService;
        public AccountController(IAccountService registerService)
        {
            this.registerService = registerService;
        }
         

        public IActionResult Register(string user)
        {
            var model = new Register();
            model.user_id = " ";

            return View(model);
        }

        public IActionResult Login(string user)
        {
            var model = new Register();
            model.user_id = " ";
            return View(model);
        }

        public JsonResult GetUser(string userId, string email, string password,string phone_number)
        { 
            var a = registerService.getUsers(userId, email, password,phone_number);
            return Json(a);
        }

        public JsonResult Add(string email,string email_phone_number, string password, string name, string phone_number, string aciklama, string gönderiSayisi,
            string takipEdilenSayisi, string takipciSayisi, string webSitesi, string userId, string username)
        {
            //if (password == "") password = null;
            //if (name == "") name = null;
            //if (username == "") username = null;
            //if (phone_number == "") phone_number = null;
            //if (aciklama == "") aciklama = null;
            //if (takipEdilenSayisi == "") takipEdilenSayisi = null;
            //if (takipciSayisi == "") takipciSayisi = null;
            //if (webSitesi == "") webSitesi = null;

            var register = registerService._add(email,email_phone_number, password, name, phone_number, aciklama, gönderiSayisi,
            takipEdilenSayisi, takipciSayisi, webSitesi, userId, username);
            return Json(register);
        }
    }
}