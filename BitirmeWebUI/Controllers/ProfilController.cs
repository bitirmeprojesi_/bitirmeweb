﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Bitirme.Business.Abstract.ServiceManager;
using Bitirme.Entity.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Bitirme.WebUI.Controllers
{
    public class ProfilController : Controller
    {
        private IAccountService accountService;
        private IProfilService profilService;
        private IProfilPostService profilPostService;
       
        public ProfilController(IAccountService accountService, IProfilService profilService, IProfilPostService profilPostService)
        {
            this.profilService = profilService;
            this.profilPostService = profilPostService;
            this.accountService = accountService;
        }

        public JsonResult AddLike(string post_id, string user_id, string yorum, string yorum_begeni, long yorum_tarih, string hash_code)
        {

            if (yorum_begeni == "1")
            {
                yorum_begeni = "-1";
            }

            var like = profilPostService.addLike(post_id, user_id, yorum, yorum_begeni, yorum_tarih, hash_code);
            return Json(like);
        }

        public IActionResult Index(string userId)
        {
            var a = accountService.user("users/", userId);
            return View(a);
        }

        public IActionResult Profil(string userId)
        {
            if (userId == null)
            {
                var a = accountService.user("users/", "id");

                return View(a);

            }
            else
            {
                var model = accountService.user("users/", userId);
                var detail = accountService.user_detail("users/", userId);
                if (detail.webSitesi == "null") { model.webSitesi = " "; }
                else { model.webSitesi = detail.webSitesi; }
                if (detail.aciklama == "null") { model.aciklama = " "; }
                else { model.aciklama = detail.aciklama; }
                model.password = model.password;
                return View(model);
            }
        }

        public JsonResult Update(Register form)
        {
            return Json(profilService.Update(form.user_id,form));
        }

        public JsonResult Kontrol(string user_name,string user_id,string phone_number)
        {
            return Json(profilService.kullaniciadiKontrol(user_name, user_id,phone_number));
        }

        public JsonResult GetPost(string user_id,string post_id)
        {
            var post=profilPostService.GetPost(user_id,post_id);
            return Json(post);
        }

        public JsonResult GetFollowers(string user_id, string type)
        {
            var post = profilPostService.GetFollowers(user_id, type);
            return Json(post);
        }


        public JsonResult OnayKaydet(string user_id, string post_id,string durum)
        {
            var post = profilPostService.OnayKaydet(user_id, post_id,durum);
            return Json(post);
        }

        public JsonResult GetComments(string post_id)
        {
            var comments = profilPostService.GetCommentList(post_id);          
            return Json(comments);
        }

        public JsonResult AddComments(string post_id, string user_id, string yorum, string yorum_begeni, long yorum_tarih, string hash_code)
        {
            var comment = profilPostService.addComment(post_id, user_id, yorum, yorum_begeni, yorum_tarih, hash_code);
            return Json(comment);
        }

      

        public JsonResult GetUserInfo(string user_id)
        {
            var userdata = profilPostService.getUserInfo(user_id);
            return Json(userdata);
        }

        public JsonResult GetUserList(string[] user_id)
        {

            var list = new Dictionary<string, string>();
            list = profilPostService.userList(user_id);
            return Json(list);
        }

        public void DeletePost(string user_id,string post_id,string gonderiSayisi)
        {
             profilPostService.deletePosts(user_id,post_id,gonderiSayisi);
         
        }
       
    }
}